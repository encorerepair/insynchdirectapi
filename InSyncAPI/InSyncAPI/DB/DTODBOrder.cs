﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InSyncAPI.Models;

namespace InSyncAPI.DB
{
    class DBDTOOrder
    {
        public string sales_order { get; set; }
        public string order { get; set; }
        public DateTime order_date { get; set; }
        public string customer { get; set; }
        public string customer_po_no { get; set; }
        public string bill_to_name { get; set; }
        public string ship_to_name { get; set; }
        public string ship_to_address_1 { get; set; }
        public string ship_to_address_2 { get; set; }
        public string ship_to_address_3 { get; set; }
        public string ship_to_city { get; set; }
        public string ship_to_state { get; set; }
        public string ship_to_zip_code { get; set; }
        public string ship_to_country_code { get; set; }
    }
    
}