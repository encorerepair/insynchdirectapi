﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InSyncAPI.Models;

namespace InSyncAPI.DB
{
    class DBIterator
    {
        public static Order OrderFromDTO(DBDTOOrder inorder)
        {
            if (inorder.sales_order == null)
                return new Order { request_status  = "Error"} ;

            var result = new Order
            {
                sales_order = inorder.sales_order,
                order = inorder.order,
                order_date = inorder.order_date.ToString(),
                customer = inorder.customer,
                customer_po_no = inorder.customer_po_no,
                bill_to_name = inorder.bill_to_name,
                address = new OrderAddress
                {
                    ship_to_name = inorder.ship_to_name,
                    ship_to_address_1 = inorder.ship_to_address_1,
                    ship_to_address_2 = inorder.ship_to_address_2,
                    ship_to_address_3 = inorder.ship_to_address_3,
                    ship_to_city = inorder.ship_to_city,
                    ship_to_state = inorder.ship_to_state,
                    ship_to_zip_code = inorder.ship_to_zip_code,
                    ship_to_country_code = inorder.ship_to_country_code,
                },
                request_status = "Success"
                
            };

            return result;
        }

        public static List<POLine> QueryPOLines(string PONumber)
        {
            var DB = new DBManager();

            List<DbParameter> parameters = new List<DbParameter>();

            parameters.Add(new DbParameter("PONumber", System.Data.ParameterDirection.Input, PONumber));

            var dbresult = DB.ExecuteList<POLine>("Integration_GetPOLines", parameters);

            if ((dbresult.Count == 1 && dbresult[0].POQty == null) || dbresult.Count <= 0)
                return null;
            
            return dbresult;
        }

        public static List<Order> QueryOrderbyCondition(string salesorder, string condition)
        {
            var DB = new DBManager();

            List<DbParameter> parameters = new List<DbParameter>();

            parameters.Add(new DbParameter("order", System.Data.ParameterDirection.Input, salesorder));
            parameters.Add(new DbParameter("condition", System.Data.ParameterDirection.Input, condition));

            var dbresult = DB.ExecuteList<DBDTOOrder>("Integration_GetOrdesbyCondition", parameters);

            if ((dbresult.Count == 1 && dbresult[0].sales_order == null) || dbresult.Count <= 0)
                return null;

            var result = new List<Order>();

            foreach (var order in dbresult)
            {
                var itemresult = OrderFromDTO(order);
                itemresult.order_lines = QueryLine(itemresult.sales_order);
                result.Add(itemresult);
            }

            return result;
        }

        public static Order QuerySalesOrder(string salesorder)
        {
            var DB = new DBManager();

            List<DbParameter> parameters = new List<DbParameter>();

            parameters.Add(new DbParameter("salesorder", System.Data.ParameterDirection.Input, salesorder));

            var dbresult = DB.ExecuteSingle<DBDTOOrder>("Integration_GetSalesOrder", parameters);

            var result = OrderFromDTO(dbresult);

            if (result == null)
                return null;

            if(result.sales_order != null && result.sales_order != "")
                result.order_lines = QueryLine(salesorder);

            return result;
        }

        public static List<OrderLine> QueryLine(string salesorder)
        {
            var DB = new DBManager();

            List<DbParameter> parameters = new List<DbParameter>();

            parameters.Add(new DbParameter("salesorder", System.Data.ParameterDirection.Input, salesorder));

            var result = DB.ExecuteList<OrderLine>("Integration_GetSalesOrderLines", parameters);


            return result;
        }

        public static  WareHouses QueryWareHouses(string salesorder)
        {
            var DB = new DBManager();

            List<DbParameter> parameters = new List<DbParameter>();

            parameters.Add(new DbParameter("salesorder", System.Data.ParameterDirection.Input, salesorder));

            var result = DB.ExecuteList<WareHouse>("Integration_GetSalesOrderWareHouses", parameters);

            if (result.Count <= 0)
                return new WareHouses { request_status = "Error" } ;

            var finalresult = new WareHouses { request_status = "Success", warehouses = new List<string>()};

            foreach (var item in result){
                finalresult.warehouses.Add(item.warehouse);
            }

            return finalresult;
        }

        public static WareHousesDev QueryWareHousesDev(string salesorder)
        {
            var DB = new DBManager();

            List<DbParameter> parameters = new List<DbParameter>();

            parameters.Add(new DbParameter("salesorder", System.Data.ParameterDirection.Input, salesorder));

            var result = DB.ExecuteList<WareHouseDev>("Integration_GetSalesOrderWareHouses_Dev", parameters);

            if (result.Count <= 0)
                return new WareHousesDev { request_status = "Error" };

            var finalresult = new WareHousesDev { request_status = "Success", warehouses = new List<WareHouseDev>() };

            foreach (var item in result)
            {
                WareHouseDev wr = new WareHouseDev();
                wr.code = item.code;
                wr.qty = item.qty;
                finalresult.warehouses.Add(wr);
            }

            return finalresult;
        }
    }
}
