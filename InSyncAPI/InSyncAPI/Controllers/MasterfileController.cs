﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using InSyncAPI.DB;
using InSyncAPI.Models;
using InSyncAPI.Filters;

namespace InSyncAPI.Controllers
{
    [BasicAuthentication]
    public class MasterfileController : ApiController
    {   
        [Route("get_description_from_masterfile")]
        [HttpGet]
        public IHttpActionResult GetDescription(string sku)
        {
            var DB = new DBManager();

            List<DbParameter> parameters = new List<DbParameter>();

            parameters.Add(new DbParameter("itemcode", System.Data.ParameterDirection.Input, sku));

            var dbresult = DB.ExecuteSingle<MasterItem>("Integration_GetItemDesc", parameters);

            if(dbresult.description == null)
                return Ok(new RequestMessage{ message = "fail", message_error = "Not Found", description = "" });

            return Ok(new RequestMessage{ message = "success", message_error = "", description = dbresult.description});
        }
    }
}
