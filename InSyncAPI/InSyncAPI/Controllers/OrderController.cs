﻿using System.Collections.Generic;
using System.Web.Http;
using InSyncAPI.DB;
using InSyncAPI.Filters;
using InSyncAPI.Models;

namespace InSyncAPI.Controllers
{
    [BasicAuthentication]
    public class OrderController: ApiController
    {


        [Route("order/{Order}/condition/{Condition}")]
        [HttpGet]
        public IHttpActionResult OrderInfoByCondition(string Order, string Condition)
        {
            var data = DBIterator.QueryOrderbyCondition(Order, Condition);

            if (data == null)
                return Ok(new RequestResp { request_status = "Empty" });
            
            return Ok(data);
        }
        
    }
}
