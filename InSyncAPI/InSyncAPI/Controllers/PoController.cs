﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using InSyncAPI.DB;
using InSyncAPI.Models;
using InSyncAPI.Filters;

namespace InSyncAPI.Controllers
{
    [BasicAuthentication]
    public class PoController : ApiController
    {
        
        public IHttpActionResult Get(string id)
        {
            string Result = "";

            var data = DBIterator.QueryPOLines(id);

            if (data == null)
                return Ok("");

            foreach (POLine obj in data)
            {
                if (Result == "")
                    Result = obj.Model + "~" + obj.ModelDesc + "~" + obj.WarehouseCode + "~" + obj.POQty + "~" + obj.UnitCost + "~" + obj.SequenceNO;
                else
                    Result = Result + "||" + obj.Model + "~" + obj.ModelDesc + "~" + obj.WarehouseCode + "~" + obj.POQty + "~" + obj.UnitCost + "~" + obj.SequenceNO;
            }

            return Ok(Result);
        }
    }
}
