﻿using System.Collections.Generic;
using System.Web.Http;
using InSyncAPI.DB;
using InSyncAPI.Filters;

namespace InSyncAPI.Controllers
{   
    [BasicAuthentication]
    public class SalesorderController : ApiController
    {
        public IHttpActionResult Get(string id) => Ok(DBIterator.QuerySalesOrder(id));


        [Route("salesorder/{salesOrder}/warehouses")]
        [HttpGet]
        public IHttpActionResult SalesOrderWareHouses(string salesOrder) => Ok(DBIterator.QueryWareHouses(salesOrder));

        [Route("dev/salesorder/{salesOrder}/warehouses")]
        [HttpGet]
        public IHttpActionResult SalesOrderWareHousesDev(string salesOrder) => Ok(DBIterator.QueryWareHousesDev(salesOrder));

    }
}
