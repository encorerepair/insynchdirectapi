﻿using System;
using Microsoft.Owin.Hosting;

namespace InSyncAPI
{
    public class InsyncService
    {


        private IDisposable _webapp;

        public void Start()
        {
            string baseAddress = Properties.Settings.Default.Server;

                // Start OWIN host 
                _webapp = WebApp.Start<Startup>(url: baseAddress);
        }

        public void Stop()
        {
            _webapp?.Dispose();
        }

    }
}
