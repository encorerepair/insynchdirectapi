﻿using Microsoft.Owin.Hosting;
using System;
using System.Net.Http;
using Topshelf;

namespace InSyncAPI
{
    class Program
    {
        static void Main(string[] args)
        {

            HostFactory.Run(x =>
            {
                x.Service<InsyncService>(s =>
                {
                    s.ConstructUsing(name => new InsyncService());
                    s.WhenStarted(tc => tc.Start());
                    s.WhenStopped(tc => tc.Stop());
                });
                x.RunAsLocalSystem();

                x.SetDescription("This is the API to Sync MASS Info in FINIX ");
                x.SetDisplayName("InSyncAPI");
                x.SetServiceName("InSyncAPI");
            });
        }
    }
}
