﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InSyncAPI.Models
{
    public class WareHouses
    {
        public string request_status { get; set; }
        public List<string> warehouses { get; set; }
        
    }

    public class WareHouse
    {
        public string warehouse { get; set; }
    }


    public class WareHousesDev
    {
        public string request_status { get; set; }
        public List<WareHouseDev> warehouses { get; set; }

    }

    public class WareHouseDev
    {
        public string code { get; set; }
        public Decimal qty { get; set; }
    }

}
