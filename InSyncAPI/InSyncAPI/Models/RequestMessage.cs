﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InSyncAPI.Models
{
    public class RequestMessage
    {
        public string message { get; set; }
        public string message_error { get; set; }
        public string description { get; set; }
    }

    public class RequestResp
    {
        public string request_status { get; set; }
    }

}
