﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InSyncAPI.DB;

namespace InSyncAPI.Models
{
    public class OrderLine
    {
        public string sales_order { get; set; }
        public string sequence { get; set; }
        public string sku { get; set; }
        public string warehouse { get; set; }
        public decimal quantity { get; set; }
        public decimal price { get; set; }

    }
}
