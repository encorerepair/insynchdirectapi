﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InSyncAPI.DB;

namespace InSyncAPI.Models
{
    public class OrderAddress
    {
        
        public string ship_to_name { get; set; }
        public string ship_to_address_1 { get; set; }
        public string ship_to_address_2 { get; set; }
        public string ship_to_address_3 { get; set; }
        public string ship_to_city { get; set; }
        public string ship_to_state { get; set; }
        public string ship_to_zip_code { get; set; }
        public string ship_to_country_code { get; set; }
    }
}
