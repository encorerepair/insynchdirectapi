﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InSyncAPI.DB;

namespace InSyncAPI.Models
{
    public class Order
    {
        public string sales_order { get; set; }
        public string order { get; set; }
        public string order_date { get; set; }
        public string customer { get; set; }
        public string customer_po_no { get; set; }
        public string bill_to_name { get; set; }
        public OrderAddress address { get; set; }
        public List<OrderLine> order_lines { get; set; }
        public string request_status { get; set; }

    }
}
