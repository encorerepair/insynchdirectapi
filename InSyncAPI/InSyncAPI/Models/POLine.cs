﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InSyncAPI.Models
{
    class POLine
    {
        public string Model { get; set; }
        public string SequenceNO { get; set; }
        public string ModelDesc { get; set; }
        public string WarehouseCode { get; set; }
        public int POQty { get; set; }
        public decimal UnitCost { get; set; }
    }
}
