﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;


namespace InSyncAPI.Models
{

    public class MailsOut
    {
        public int mailoutid { get; set; }
        public string fromaddress { get; set; }
        public string toaddresses { get; set; }
    }
}
