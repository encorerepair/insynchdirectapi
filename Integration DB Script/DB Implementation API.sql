/****** Object:  Table [dbo].[SO_SalesOrderHistoryHeader]    Script Date: 12/11/2018 9:08:44 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Integration_CustomerConditionMapping](
	[CustomerNo] [varchar](20),
	[Condition] [varchar](255) NULL
 CONSTRAINT [KPRIMARY_Integration_CustomerConditionMapping] PRIMARY KEY CLUSTERED 
(
	[CustomerNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO




INSERT INTO [Integration_CustomerConditionMapping] VALUES('0006276','GOOD');
INSERT INTO [Integration_CustomerConditionMapping] VALUES('0006336','GOOD');
INSERT INTO [Integration_CustomerConditionMapping] VALUES('0006518','GOOD');
INSERT INTO [Integration_CustomerConditionMapping] VALUES('0006297','RUR');
INSERT INTO [Integration_CustomerConditionMapping] VALUES('0006668','GOOD');
INSERT INTO [Integration_CustomerConditionMapping] VALUES('0006712','GOOD');
INSERT INTO [Integration_CustomerConditionMapping] VALUES('0006704','GOOD');
INSERT INTO [Integration_CustomerConditionMapping] VALUES('0006707','GOOD');
INSERT INTO [Integration_CustomerConditionMapping] VALUES('0006722','GOOD');


/****** Object:  StoredProcedure [dbo].[Integration_GetPOLines]    Script Date: 1/16/2019 9:08:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elisardo Felix Vasquez
-- Create date: 2019-01-14
-- Description:	Obtain PO lines by PO Number
-- =============================================
CREATE PROCEDURE [dbo].[Integration_GetPOLines] (
	@PONumber varchar(255)
)
AS
BEGIN
	SELECT 
		pod.[ItemCode] as Model, 
		pod.[LineKey] as SequenceNO,
		pod.[ItemCodeDesc] as ModelDesc,
		pod.[WarehouseCode],
		Cast(pod.[QuantityOrdered] as int) as POQty,
		pod.[UnitCost]
	FROM [MAS_ENC].[dbo].[PO_PurchaseOrderDetail] as pod 
	JOIN  [dbo].[PO_PurchaseOrderHeader] as poh ON pod.[PurchaseOrderNo] = poh.[PurchaseOrderNo]
	WHERE pod.[PurchaseOrderNo] = @PONumber
	AND [ItemCode] != '/C';
END


/****** Object:  StoredProcedure [dbo].[Integration_GetItemDesc]    Script Date: 1/17/2019 8:41:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elisardo Felix Vasquez
-- Create date: 2018-11-30
-- Description:	Obtain the ItemCode Description
-- =============================================
CREATE PROCEDURE [dbo].[Integration_GetItemDesc] (
	@itemcode varchar(255)
)
AS
BEGIN
  SELECT TOP 1
      [ItemCodeDesc] as description
  FROM [MAS_ENC].[dbo].[CI_Item]
  WHERE ItemCode = @itemcode;
END

/****** Object:  StoredProcedure [dbo].[Integration_GetOrdesbyCondition]    Script Date: 1/17/2019 8:44:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elisardo Felix Vasquez
-- Create date: 2018-11-29
-- Description:	Obtain Sales Order By SO number
-- =============================================
CREATE PROCEDURE [dbo].[Integration_GetOrdesbyCondition] (
	@order varchar(255),
	@condition varchar(255)

)
AS
BEGIN
	SELECT TOP 1 
	   [SalesOrderNo] as sales_order
	  ,[ConfirmTo] as [order]
      ,[OrderDate] as order_date
      ,[OrderStatus] as order_status
      ,so.[CustomerNo] as customer
      ,[CustomerPONo] as customer_po_no
	  ,[BillToName] as bill_to_name
	  ,[ShipToName] as ship_to_name
      ,[ShipToAddress1] as ship_to_address_1
      ,[ShipToAddress2] as ship_to_address_2
      ,[ShipToAddress3] as ship_to_address_3
      ,[ShipToCity] as ship_to_city
      ,[ShipToState] as ship_to_state
      ,[ShipToZipCode] as ship_to_zip_code
      ,[ShipToCountryCode] as ship_to_country_code
  FROM [MAS_ENC].[dbo].[SO_SalesOrderHistoryHeader] as so JOIN [dbo].[Integration_CustomerConditionMapping] as cm on so.CustomerNo = cm.CustomerNo
  WHERE [ConfirmTo] = @order
  AND cm.condition = @condition
  AND [OrderStatus] = 'A';
END

/****** Object:  StoredProcedure [dbo].[Integration_GetSalesOrder]    Script Date: 1/17/2019 8:44:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elisardo Felix Vasquez
-- Create date: 2018-11-29
-- Description:	Obtain Sales Order By SO number
-- =============================================
CREATE PROCEDURE [dbo].[Integration_GetSalesOrder] (
	@salesorder varchar(255)
)
AS
BEGIN
	SELECT TOP 1 
	   [SalesOrderNo] as sales_order
	  ,[ConfirmTo] as [order]
      ,[OrderDate] as order_date
      ,[OrderStatus] as order_status
      ,[CustomerNo] as customer
      ,[CustomerPONo] as customer_po_no
	  ,[BillToName] as bill_to_name
	  ,[ShipToName] as ship_to_name
      ,[ShipToAddress1] as ship_to_address_1
      ,[ShipToAddress2] as ship_to_address_2
      ,[ShipToAddress3] as ship_to_address_3
      ,[ShipToCity] as ship_to_city
      ,[ShipToState] as ship_to_state
      ,[ShipToZipCode] as ship_to_zip_code
      ,[ShipToCountryCode] as ship_to_country_code
  FROM [MAS_ENC].[dbo].[SO_SalesOrderHistoryHeader]
  WHERE [SalesOrderNo] = @salesorder
  AND [OrderStatus] = 'A';
END

/****** Object:  StoredProcedure [dbo].[Integration_GetSalesOrderLines]    Script Date: 1/17/2019 8:45:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elisardo Felix Vasquez
-- Create date: 2018-11-29
-- Description:	Obtain Sales Order Wharehouse By SO number
-- =============================================
CREATE PROCEDURE [dbo].[Integration_GetSalesOrderLines] (
	@salesorder varchar(255)
)
AS
BEGIN
  SELECT Distinct [SalesOrderNo] as sales_order
      ,[SequenceNo] as sequence
      ,[CancelledLine] as cancelled_line
      ,REPLACE([ItemCode],'_RUR', '') as sku
      ,[WarehouseCode] as warehouse
      ,[QuantityOrderedRevised] as quantity
      ,[QuantityShipped] as qty_shipped
      ,[LastUnitPrice] as price
  FROM [MAS_ENC].[dbo].[SO_SalesOrderHistoryDetail]
  WHERE [SalesOrderNo] = @salesorder
  AND [CancelledLine] = 'N';
END


/****** Object:  StoredProcedure [dbo].[Integration_GetSalesOrderWhareHouses]    Script Date: 11/30/2018 12:15:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Elisardo Felix Vasquez
-- Create date: 2018-11-29
-- Description:	Obtain Sales Order Wharehouse By SO number
-- =============================================
CREATE PROCEDURE [dbo].[Integration_GetSalesOrderWareHouses] (
	@salesorder varchar(255)
)
AS
BEGIN
  SELECT DISTINCT
		  [WarehouseCode] as warehouse
  FROM [MAS_ENC].[dbo].[SO_SalesOrderHistoryDetail]
  WHERE [SalesOrderNo] = @salesorder
  AND [WarehouseCode] <> ''
  AND [WarehouseCode] IS NOT NULL
  AND [CancelledLine] = 'N';
END


/****** Object:  StoredProcedure [dbo].[Integration_GetSalesOrderWareHouses_Dev]    Script Date: 3/18/2019 11:19:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Elisardo Felix Vasquez
-- Create date: 2018-11-29
-- Description:	Obtain Sales Order Wharehouse By SO number
-- =============================================
CREATE PROCEDURE [dbo].[Integration_GetSalesOrderWareHouses_Dev] (
	@salesorder varchar(255)
)
AS
BEGIN
  SELECT
		  [WarehouseCode] as code,
		  sum([QuantityOrderedRevised]) as qty
  FROM [MAS_ENC].[dbo].[SO_SalesOrderHistoryDetail]
  WHERE [SalesOrderNo] = @salesorder
  AND [WarehouseCode] <> ''
  AND [WarehouseCode] IS NOT NULL
  AND [CancelledLine] = 'N'
  GROUP BY [WarehouseCode];
END
